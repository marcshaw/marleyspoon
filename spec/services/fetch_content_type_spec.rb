require 'rails_helper'

describe FetchContentType, type: :service do
  subject(:fetch_content_type) { described_class.new(content_type: content_type).call }
  let(:content_type) { 'recipe' }
  let(:content_client) { double }

  before do
    stub_const('CONTENTFUL_CLIENT', content_client)
  end

  context 'when content_type is valcontent_type' do
    it 'calls the contentful client' do
      expect(content_client).to receive(:entries).with(content_type: content_type)

      fetch_content_type
    end
  end

  context 'when content_type is invalcontent_type' do
    context 'when content_type is null' do
      let(:content_type) { nil }

      it 'raises an exception' do
        expect { fetch_content_type }.to raise_error(ArgumentError, /content type can not be blank/)
        expect(content_client).to_not receive(:entries).with(content_type: content_type)
      end
    end

    context 'when content_type is blank' do
      let(:content_type) { '' }

      it 'raises an exception' do
        expect { fetch_content_type }.to raise_error(ArgumentError, /content type can not be blank/)
        expect(content_client).to_not receive(:entries).with(content_type: content_type)
      end
    end
  end
end
