require 'rails_helper'

describe ExtractRecipeFeatures, type: :service do
  subject(:extract_features) { described_class.new(recipe: recipe).call }
  let(:recipe) { double(**attributes) }
  let(:photo) { double(image_url: 'x') }
  let(:tags) { [double(name: 'a'), double(name: 'b')] }
  let(:chef) { double(name: 'l') }

  let(:default_attributes) do
    {
      title: 'm',
      photo: photo,
      description: 'n'
    }
  end

  context 'when it has all the attributes' do
    let(:attributes) { default_attributes.merge(tags: tags, chef: chef) }
    let(:expected_result) do
      {
        title: 'm',
        photo: 'x',
        description: 'n',
        tags: ['a', 'b'],
        chefName: 'l'
      }
    end

    it 'extracts the expected features' do
      expect(extract_features).to eq expected_result
    end
  end

  context 'when it does not have tags' do
    let(:attributes) { default_attributes.merge(chef: chef) }
    let(:expected_result) do
      {
        title: 'm',
        photo: 'x',
        description: 'n',
        chefName: 'l'
      }
    end

    it 'extracts the expected features' do
      expect(extract_features).to eq expected_result
    end
  end

  context 'when it does not have a chef' do
    let(:attributes) { default_attributes.merge(tags: tags) }
    let(:expected_result) do
      {
        title: 'm',
        photo: 'x',
        description: 'n',
        tags: ['a', 'b'],
      }
    end

    it 'extracts the expected features' do
      expect(extract_features).to eq expected_result
    end
  end
end
