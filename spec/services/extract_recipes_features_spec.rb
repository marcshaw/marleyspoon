require 'rails_helper'

describe ExtractRecipesFeatures, type: :service do
  subject(:extract_features) { described_class.new(recipes: recipes).call }
  let(:recipes) { [recipe_1, recipe_2] }
  let(:recipe_1) { double(photo: photo_1, id: 1, title: 'a') }
  let(:recipe_2) { double(photo: photo_2, id: 2, title: 'b') }
  let(:photo_1) { double(image_url: 'x') }
  let(:photo_2) { double(image_url: 'z') }

  let(:expected_result) do
    [
      { id: 1, title: 'a', photo: 'x' },
      { id: 2, title: 'b', photo: 'z' }
    ]
  end

  it 'extracts the expected features' do
    expect(extract_features).to eq expected_result
  end
end
