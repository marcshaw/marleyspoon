require 'rails_helper'

describe FetchEntry, type: :service do
  subject(:fetch_entry) { described_class.new(id: id).call }
  let(:id) { 1 }
  let(:content_client) { double }

  before do
    stub_const('CONTENTFUL_CLIENT', content_client)
  end

  context 'when id is valid' do
    it 'calls the contentful client' do
      expect(content_client).to receive(:entry).with(id)

      fetch_entry
    end
  end

  context 'when id is invalid' do
    context 'when id is null' do
      let(:id) { nil }

      it 'raises an exception' do
        expect { fetch_entry }.to raise_error(ArgumentError, /id can not be blank/)
        expect(content_client).to_not receive(:entry).with(id)
      end
    end

    context 'when id is blank' do
      let(:id) { '' }

      it 'raises an exception' do
        expect { fetch_entry }.to raise_error(ArgumentError, /id can not be blank/)
        expect(content_client).to_not receive(:entry).with(id)
      end
    end
  end
end
