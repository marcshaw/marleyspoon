require 'rails_helper'

describe RecipesController, type: :request do
  context '#index' do
    subject(:request_index) { get recipes_path }
    let(:content_type) { double(call: features) }
    let(:features) { double(call: {})  }

    it 'calls the expected serviecs' do
      expect(FetchContentType).to receive(:new).with(content_type: 'recipe').and_return(content_type)
      expect(ExtractRecipesFeatures).to receive(:new).with(recipes: features).and_return(features)

      request_index
    end
  end

  context '#show' do
    subject(:request_show) { get recipe_path(id: id) }

    let(:id) { '1' }
    let(:entry) { double(call: features) }
    let(:features) { double(call: {}) }

    it 'calls the expected serviecs' do
      expect(FetchEntry).to receive(:new).with(id: id).and_return(entry)
      expect(ExtractRecipeFeatures).to receive(:new).with(recipe: features).and_return(features)

      request_show
    end
  end
end
