require 'rails_helper'

describe RecipesController, type: :request do
  context '#index' do
    subject(:request_index) { get recipes_path }

    it 'responds successfully' do
      request_index

      expect(response).to be_successful
    end
  end

  context '#show' do
    subject(:request_show) { get recipe_path(id: '2E8bc3VcJmA8OgmQsageas') }

    it 'responds successfully' do
      request_show

      expect(response).to be_successful
    end
  end
end
