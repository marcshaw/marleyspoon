# README

# Thoughts:
I was thinking I could do the whole request logic in React, but since it is for a backend role, I decided to do it in ruby.

I had to create the extract feature classes because the react gem calls `as_json` on the entries from ContentFul, and this was causing a stack overflow. So I had to extract the features into a hash before passing them into the react gem.

# TODO's:
* Maybe we could use some caching. For example, we cache the results of recipes for 30 minutes (?). If we call show, we first look in the cache, otherwise we call contentful. To do the caching we could create an active job, which will cache it async.
* Tidy up UI

# To get running:
* Have ruby 2.6.3 installed, maybe through rbenv
* run `bundle install`
* run `npm install`
* run `bundle exec rails s`  (I have pushed up the .env to make it easier to run)
* navigate to `localhost:3000/recipes`
