CONTENTFUL_CLIENT = Contentful::Client.new(
  space: ENV['CONTENTFUL_SPACE'],  # This is the space ID. A space is like a project folder in Contentful terms
  access_token: ENV['CONTENTFUL_TOKEN'],  # This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  entry_mapping: {
    'recipe' => Recipe
  }
)
