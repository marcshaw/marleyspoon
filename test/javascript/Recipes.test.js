import React from 'react';
import ReactDOM from 'react-dom';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import Recipes from '../../app/javascript/bundles/Recipes/components/Recipes';

test('rendered component', () => {
  const recipes = [{ title: 'a', photo: 'asdfad', id: '1'}],
        wrapper = shallow(<Recipes recipes={recipes}/>);

  expect(wrapper).toMatchSnapshot();
});
