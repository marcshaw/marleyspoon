import React from 'react';
import ReactDOM from 'react-dom';

import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import Recipe from '../../app/javascript/bundles/Recipes/components/Recipe';

test('rendered component', () => {
  const recipe = { title: 'a', description: 'a', photo: 'asdfad', chefName: 'aa', tags: 'as'},
        wrapper = shallow(<Recipe recipe={recipe}/>);

  expect(wrapper).toMatchSnapshot();
});
