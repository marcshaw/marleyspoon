class RecipesController < ApplicationController
  layout 'recipes'

  def index
    @recipes_props = { recipes: recipes }
  end

  def show
    @recipe_props = { recipe: recipe }
  end

  private

  def recipes
    entries = FetchContentType.new(content_type: 'recipe').call
    ExtractRecipesFeatures.new(recipes: entries).call
  end

  def recipe
    entry = FetchEntry.new(id: params[:id]).call
    ExtractRecipeFeatures.new(recipe: entry).call
  end
end
