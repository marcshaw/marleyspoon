class ExtractRecipesFeatures
  def initialize(recipes:)
    @recipes = recipes
  end

  def call
    recipes.map do |entry|
      {
        id: entry.id,
        title: entry.title,
        photo: entry.photo.image_url(width: 400, height: 400)
      }
    end
  end

  private

  attr_reader :recipes
end
