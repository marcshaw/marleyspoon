class FetchContentType
  def initialize(content_type:)
    @content_type = content_type
  end

  def call
    raise ArgumentError, 'content type can not be blank' if content_type.blank?

    ::CONTENTFUL_CLIENT.entries(content_type: content_type)
  end

  private

  attr_reader :content_type
end
