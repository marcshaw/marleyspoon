class FetchEntry
  def initialize(id:)
    @id = id
  end

  def call
    raise ArgumentError, 'id can not be blank' if id.blank?

    ::CONTENTFUL_CLIENT.entry(id)
  end

  private

  attr_reader :id
end
