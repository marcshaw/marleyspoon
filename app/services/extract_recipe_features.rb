class ExtractRecipeFeatures
  def initialize(recipe:)
    @recipe = recipe
  end

  def call
    {
      title: recipe.title,
      photo: recipe.photo.image_url(width: 400, height: 400),
      description: recipe.description,
      tags: (tags if tags?),
      chefName: (chef_name if chef?)
    }.compact
  end

  private

  attr_reader :features, :recipe

  def tags
    recipe.tags.map(&:name)
  end

  def chef_name
    recipe.chef.name
  end

  def tags?
    recipe.respond_to?(:tags)
  end

  def chef?
    recipe.respond_to?(:chef)
  end
end
