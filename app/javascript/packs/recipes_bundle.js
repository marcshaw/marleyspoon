import ReactOnRails from 'react-on-rails';

import Recipes from '../bundles/Recipes/components/Recipes';
import Recipe from '../bundles/Recipes/components/Recipe';

ReactOnRails.register({
  Recipes,
  Recipe
});
