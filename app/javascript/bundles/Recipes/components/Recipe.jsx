import React from 'react';

export default class Recipe extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, photo, description, chefName, tags } = this.props.recipe

    return (
      <div className='recipe'>
        <div className='title'>
          <h3> { title } </h3>
        </div>
        <div className='photo'>
          <img src={ photo }/>
        </div>
        <div className='description'>
          <h4> Description: </h4>
          { description }
        </div>
        <div className='chef'>
          <h4> Chef: </h4>
          { chefName }
        </div>
        <div className='tags'>
          <h4> Tags: </h4>
          { tags && tags.join(', ') }
        </div>
      </div>
    );
  }
}
