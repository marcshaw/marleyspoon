import React from 'react';
import Card from 'react-bootstrap/Card';

import Recipe from './Recipe';

export default class Recipes extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { recipes } = this.props;

    return (
      <div className='recipes'>
        { recipes.map((recipe, index) => {
          return (
            <Card style={{ width: '25rem' }}>
            <a className='recipe' href={`recipes/${recipe.id}`}>
              <Card.Img variant="top" src={ recipe.photo } />
              <Card.Body>
                <Card.Title>{ recipe.title }</Card.Title>
              </Card.Body>
            </a>
          </Card>
          )
        })}
      </div>
    );
  }
}
